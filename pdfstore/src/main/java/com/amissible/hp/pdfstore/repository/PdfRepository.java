package com.amissible.hp.pdfstore.repository;

import com.amissible.hp.pdfstore.models.Pdf;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PdfRepository extends JpaRepository<Pdf, String> {

}
