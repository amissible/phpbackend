package com.amissible.hp.pdfstore.controller;

import com.amissible.hp.pdfstore.models.OrderModel;
import com.amissible.hp.pdfstore.models.Pdf;
import com.amissible.hp.pdfstore.repository.PdfRepository;
import com.amissible.hp.pdfstore.service.InvoiceService;
import com.amissible.hp.pdfstore.service.MockOrderService;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;
import java.io.*;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;

import static java.lang.String.format;
import static org.springframework.http.MediaType.APPLICATION_PDF;

@RestController
public class PdfController {

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    MockOrderService mockOrderService;

    @Autowired
    PdfRepository repository;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public List<Pdf> save(@RequestParam("file") MultipartFile file) {
        try {
            Pdf pdf = new Pdf();
            pdf.setContent(file.getBytes());
            pdf.setContentType(file.getContentType());
            pdf.setFilename(file.getOriginalFilename());
            repository.save(pdf);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return repository.findAll();
    }

    // generate invoice pdf
    @PostMapping(value = "/generate", produces = "application/pdf")
    public ResponseEntity<InputStreamResource> invoiceGenerate(@RequestParam(name = "code", defaultValue = "XYZ123456789") String code,
                                                               @RequestParam(name = "lang", defaultValue = "en") String lang) throws IOException {
        final OrderModel order = mockOrderService.getOrderByCode(code);
        final File invoicePdf = invoiceService.generateInvoiceFor(order, Locale.forLanguageTag(lang));

        final HttpHeaders httpHeaders = getHttpHeaders(code, lang, invoicePdf);
        return new ResponseEntity<>(new InputStreamResource(new FileInputStream(invoicePdf)), HttpStatus.OK);
    }

    private HttpHeaders getHttpHeaders(String code, String lang, File invoicePdf) {
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(APPLICATION_PDF);
        respHeaders.setContentLength(invoicePdf.length());
        respHeaders.setContentDispositionFormData("attachment", format("%s-%s.pdf", code, lang));
        return respHeaders;
    }
}
