package com.amissible.hp.pdfstore.models;

import javax.persistence.*;
import java.sql.Blob;
import java.sql.Date;

@Entity
public class Pdf {

    @Id
    @GeneratedValue
    @Column(name="id")
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="filename")
    private String filename;

    @Column(name="content")
    @Lob
    private byte[] content;

    @Column(name="content_type")
    private String contentType;

    @Column(name="created")
    private Date created;

    public Pdf() {
    }

    public Pdf(Integer id, String name, String description, String filename, byte[] content, String contentType, Date created) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.filename = filename;
        this.content = content;
        this.contentType = contentType;
        this.created = created;
    }

    //Getter and Setter methods
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}