package com.amissible.hp.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Invoice {
    private static Long s_id = 0l;
    private Long invoiceId;
    private String date;
    private String dueDate;
    private CompanyDetails companyDetails;
    private BillingAddress billingAddress;
    private ShippingAddress shippingAddress;
    private List<Product> productList;
    private Float subTotal = 0.0f;
    private Float discounts = 0.0f;
    private Float taxes = 0.0f;
    private Float total = 0.0f;

    public Invoice() {
        this.invoiceId = ++s_id;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        this.date = now.toString();
    }

    public Invoice(Long invoiceId, String dueDate,
                   CompanyDetails companyDetails,
                   BillingAddress billingAddress,
                   ShippingAddress shippingAddress,
                   List<Product> productList,
                   Float discounts, Float taxes) {
        this.invoiceId = invoiceId;
        this.dueDate = dueDate;
        this.companyDetails = companyDetails;
        this.billingAddress = billingAddress;
        this.shippingAddress = shippingAddress;
        this.productList = productList;
        this.discounts = discounts;
        this.taxes = taxes;
    }

    public Invoice(String dueDate, CompanyDetails companyDetails,
                   BillingAddress billingAddress,
                   ShippingAddress shippingAddress,
                   List<Product> productList) {
        this.invoiceId = ++s_id;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        this.date = now.toString();
        this.dueDate = dueDate;
        this.companyDetails = companyDetails;
        this.billingAddress = billingAddress;
        this.shippingAddress = shippingAddress;
        this.productList = productList;
        calculateTotal();
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public CompanyDetails getCompanyDetails() {
        return companyDetails;
    }

    public void setCompanyDetails(CompanyDetails companyDetails) {
        this.companyDetails = companyDetails;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> product) {
        this.productList = product;
        calculateTotal();
    }

    public void addProduct(Product product) {
        productList.add(product);
        calculateTotal();
    }

    public Float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Float subTotal) {
        this.subTotal = subTotal;
    }

    public Float getDiscounts() {
        return discounts;
    }

    public void setDiscounts(Float discounts) {
        this.discounts = discounts;
    }

    public Float getTaxes() {
        return taxes;
    }

    public void setTaxes(Float taxes) {
        this.taxes = taxes;
    }

    public Float getTotal() {
        if(this.productList != null)
            calculateTotal();
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    void calculateTotal() {
        this.subTotal = 0.0f;
        for(Product itr : productList) {
            this.subTotal += itr.getAmount();
        }
        this.total = (this.subTotal - this.discounts) + this.taxes;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "invoiceId=" + invoiceId +
                ", date='" + date + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", companyDetails=" + companyDetails +
                ", billingAddress=" + billingAddress +
                ", shippingAddress=" + shippingAddress +
                ", productList=" + productList +
                ", subTotal=" + subTotal +
                ", discounts=" + discounts +
                ", taxes=" + taxes +
                ", total=" + total +
                '}';
    }
}
