package com.amissible.hp.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table("PDF")
public class Pdf {
    @Id
    @Column(value="id")
    private Long id;

    @Column(value="filename")
    private String filename;

    @Column(value="content")
    private byte[] content;

    @Column(value="tags")
    private String tag;

    public Pdf() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        tag = now.toString();
        //System.out.println(now.toString());
    }

    public Pdf(Long id, String filename, byte[] content) {
        this.id = id;
        this.filename = filename;
        this.content = content;
     }

    //Getter and Setter methods
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }


    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return "Pdf{" +
                "id=" + id +
                ", filename='" + filename + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}