package com.amissible.hp.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Table("INVOICE")
public class InvoiceTable {
    @Id
    @Column(value = "id")
    private Long id;
    @Column(value = "customer_name")
    private String customerName;
    @Column(value = "billed_date")
    private String date;
    @Column(value = "subtotal")
    private Float subTotal = 0.0f;
    @Column(value = "discounts")
    private Float discounts = 0.0f;
    @Column(value = "taxes")
    private Float taxes = 0.0f;
    @Column(value = "total")
    private Float total;
    @Column(value = "due_date")
    private String dueDate;

    public InvoiceTable(){
    }

    public InvoiceTable(Long id, String customerName, String date,
                        String dueDate,Float discounts, Float taxes) {
        this.id = id;
        this.customerName = customerName;
        this.date = date;
        this.dueDate = dueDate;
        this.discounts = discounts;
        this.taxes = taxes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Float subTotal) {
        this.subTotal = subTotal;
    }

    public Float getDiscounts() {
        return discounts;
    }

    public void setDiscounts(Float discounts) {
        this.discounts = discounts;
    }

    public Float getTaxes() {
        return taxes;
    }

    public void setTaxes(Float taxes) {
        this.taxes = taxes;
    }

    @Override
    public String toString() {
        return "InvoiceTable{" +
                "id=" + id +
                ", customerName='" + customerName + '\'' +
                ", date='" + date + '\'' +
                ", total=" + total +
                ", dueDate='" + dueDate + '\'' +
                ", subTotal=" + subTotal +
                ", discounts=" + discounts +
                ", taxes=" + taxes +
                '}';
    }
}
