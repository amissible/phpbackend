package com.amissible.hp.model;

import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public class InvoicePayload {
    private CompanyDetails companyDetails;
    private BillingAddress billingAddress;
    private ShippingAddress shippingAddress;
    private List<Product> productList;

    InvoicePayload(){
    }

    public InvoicePayload(CompanyDetails companyDetails, BillingAddress billingAddress, ShippingAddress shippingAddress, List<Product> productList) {
        this.companyDetails = companyDetails;
        this.billingAddress = billingAddress;
        this.shippingAddress = shippingAddress;
        this.productList = productList;
    }

    public CompanyDetails getCompanyDetails() {
        return companyDetails;
    }

    public void setCompanyDetails(CompanyDetails companyDetails) {
        this.companyDetails = companyDetails;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProduct(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "InvoicePayload{" +
                "companyDetails=" + companyDetails +
                ", billingAddress=" + billingAddress +
                ", shippingAddress=" + shippingAddress +
                ", productList=" + productList +
                '}';
    }
}
