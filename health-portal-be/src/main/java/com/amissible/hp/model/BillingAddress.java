package com.amissible.hp.model;

public class BillingAddress extends Address {
    public BillingAddress() {
    }

    public BillingAddress(String recipientName, String companyName,
                          String streetAddress, String city,
                          String state, String zipCode,
                          String phone) {
        super(recipientName, companyName, streetAddress, city, state, zipCode, phone);
    }
}
