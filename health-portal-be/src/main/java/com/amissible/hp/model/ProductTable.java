package com.amissible.hp.model;

import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;
import org.springframework.data.annotation.Id;

@Entity
@Table("PRODUCT")
public class ProductTable {
    @Id
    @Column(value = "id")
    private Long id;
    @Column(value="name")
    private String name;
    @Column(value="price")
    private Float price;

    public ProductTable() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductTable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
