package com.amissible.hp.model;

public class CompanyDetails {
    private String logoUrl;
    private String companyName;
    private String streetAddress;
    private Integer phone;
    private String email;
    private String companyWebsite;

    public CompanyDetails() {
    }

    public CompanyDetails(String logoUrl, String companyName,
                          String streetAddress, Integer phone,
                          String email, String companyWebsite) {
        this.logoUrl = logoUrl;
        this.companyName = companyName;
        this.streetAddress = streetAddress;
        this.phone = phone;
        this.email = email;
        this.companyWebsite = companyWebsite;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    @Override
    public String toString() {
        return "CompanyDetails{" +
                "logoUrl='" + logoUrl + '\'' +
                ", companyName='" + companyName + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", companyWebsite='" + companyWebsite + '\'' +
                '}';
    }
}
