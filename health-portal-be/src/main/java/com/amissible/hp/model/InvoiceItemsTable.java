package com.amissible.hp.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.Entity;

@Entity
@Table("INVOICE_ITEMS")
public class InvoiceItemsTable {
    @Id
    @Column(value = "id")
    private Long id;
    @Column(value="invoice_id")
    private Long invoiceId;
    @Column(value="product_id")
    private Long productId;
    @Column(value="quantity")
    private Integer quantity;

    public InvoiceItemsTable() {
    }

    public InvoiceItemsTable(Long id, Long invoiceId, Long productId, Integer quantity) {
        this.id = id;
        this.invoiceId = invoiceId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "InvoiceItemsTable{" +
                "id=" + id +
                ", invoiceId=" + invoiceId +
                ", productId=" + productId +
                ", quantity=" + quantity +
                '}';
    }
}
