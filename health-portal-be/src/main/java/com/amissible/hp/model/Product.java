package com.amissible.hp.model;

public class Product {
    private String description;
    private Integer quantity;
    private Float unitPrice;
    private Float amount = 0.0f;

    public Product() {
    }

    public Product(String description, Integer quantity,
                   Float unitPrice) {
        this.description = description;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Float getAmount() {
        if(this.quantity > 1)
            this.amount = unitPrice * quantity;
        else
            this.amount = this.unitPrice;
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "description='" + description + '\'' +
                ", quantity=" + quantity +
                ", unitPrice=" + unitPrice +
                ", amount=" + amount +
                '}';
    }
}
