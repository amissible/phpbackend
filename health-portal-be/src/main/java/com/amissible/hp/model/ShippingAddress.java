package com.amissible.hp.model;

public class ShippingAddress extends Address {
    ShippingAddress() {
    }

    public ShippingAddress(String recipientName, String companyName,
                           String streetAddress, String city,
                           String state, String zipCode, String phone) {
        super(recipientName, companyName, streetAddress, city, state, zipCode, phone);
    }
}
