package com.amissible.hp.service;

import com.amissible.hp.model.CompanyDetails;
import com.amissible.hp.model.Product;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {

    public String exportReport(String reportFormat, List<Product> productList, CompanyDetails companyDetails) throws FileNotFoundException, JRException {
        String path = "\\Reports";
        //load file and compile it
        File file = ResourceUtils.getFile("classpath:Invoice.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(productList);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("path", "E:\\\\Images\\\\logo.jpg");
        parameters.put("companyDetails", companyDetails);
        //parameters.put("productList", productList);
        //parameters.put("companyDetails", companyDetails);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, "Invoice.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, "Invoice.pdf");
        }

        return "report generated in path : " + path;
    }
}