package com.amissible.hp.pdf;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.amissible.hp.model.User;
import com.amissible.hp.repo.UserRepository;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PdfExporter {
    private User itr;

    @Autowired
    UserRepository userRepository;

    private void writeTableData(PdfPTable table, String email) {
        itr = userRepository.findByEmail(email).block();

        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.WHITE);
        cell.setPadding(1);
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.BLACK);
        font.setSize(12);

        cell.setPhrase(new Phrase("XID", font));
        table.addCell(cell);
        table.addCell(String.valueOf(itr.getXid()));
        cell.setPhrase(new Phrase("First Name", font));
        table.addCell(cell);
        table.addCell(itr.getFirstName());
        cell.setPhrase(new Phrase("Last Name", font));
        table.addCell(cell);
        table.addCell(itr.getLastName());
        cell.setPhrase(new Phrase("Email", font));
        table.addCell(cell);
        table.addCell(itr.getEmail());
        cell.setPhrase(new Phrase("Display Image Url", font));
        table.addCell(cell);
        table.addCell(itr.getDisplayImageUrl());
        cell.setPhrase(new Phrase("Birth Date", font));
        table.addCell(cell);
        table.addCell(itr.getBirth_date());
        cell.setPhrase(new Phrase("Gender", font));
        table.addCell(cell);
        table.addCell(itr.getGender());
        cell.setPhrase(new Phrase("Insurance Provider", font));
        table.addCell(cell);
        table.addCell(itr.getInsurance_provider());
        cell.setPhrase(new Phrase("Employer Info", font));
        table.addCell(cell);
        table.addCell(itr.getEmployer_info());
        cell.setPhrase(new Phrase("Address", font));
        table.addCell(cell);
        table.addCell(itr.getAddress());
        cell.setPhrase(new Phrase("Real Id", font));
        table.addCell(cell);
        table.addCell(itr.getRealId());
        cell.setPhrase(new Phrase("User Type", font));
        table.addCell(cell);
        table.addCell(itr.getUser_type());
    }

    public ResponseEntity<Resource> export(String email) throws DocumentException, IOException {
        String fileName = "Users.pdf";
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, new FileOutputStream(fileName));

        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.RED);

        Paragraph p = new Paragraph("User Details", font);
        p.setAlignment(Paragraph.ALIGN_CENTER);

        document.add(p);
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100f);
        table.setWidths(new float[] {1.5f, 1.5f});
        table.setSpacingBefore(10);

        //writeTableHeader(table);
        writeTableData(table, email);
        document.add(table);
        document.close();
        Path path = Paths.get(fileName);
        Resource resource = null;
        try {
            resource = new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return  ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("text/pdf"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}