package com.amissible.hp.pdf;

import com.amissible.hp.dao.EmailService;
import com.amissible.hp.model.Invoice;
import com.amissible.hp.model.Product;
import com.amissible.hp.repo.UserRepository;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class InvoiceExporter {

    @Autowired
    EmailService emailService;

    private void writeTableData(PdfPTable table, Invoice invoice) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.WHITE);
        cell.setPadding(5);
        com.lowagie.text.Font hfont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        hfont.setColor(Color.BLACK);
        hfont.setSize(12);

        com.lowagie.text.Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.DARK_GRAY);
        font.setSize(12);

        cell.setPhrase(new Phrase("Description", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Quantity", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Unit Price", font));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Amount", font));
        table.addCell(cell);

        for(Product itr : invoice.getProductList()) {
            table.addCell(itr.getDescription());
            table.addCell(itr.getQuantity().toString());
            table.addCell(itr.getUnitPrice().toString());
            table.addCell(itr.getAmount().toString());
        }
    }

    public ResponseEntity<Resource> export(String email, Invoice invoice) throws DocumentException, IOException {
        String fileName = "Invoice" + invoice.getInvoiceId() + ".pdf";
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, new FileOutputStream(fileName));

        document.open();

        //fonts
        Font fLogo = FontFactory.getFont(FontFactory.TIMES_BOLD);
        fLogo.setSize(18);
        fLogo.setColor(Color.BLUE);
        Font fInfo = FontFactory.getFont(FontFactory.TIMES);
        fInfo.setSize(12);
        fInfo.setColor(Color.BLACK);

        Paragraph header = new Paragraph("Invoice", fLogo);
        header.setAlignment(Paragraph.ALIGN_CENTER);

        Paragraph companyName = new Paragraph(invoice.getCompanyDetails().getCompanyName(), fLogo);
        companyName.setAlignment(Paragraph.ALIGN_LEFT);
        companyName.setSpacingAfter(15);

        Paragraph companyAddr = new Paragraph("Street: " + invoice.getCompanyDetails().getStreetAddress() +
                "\n" + "Phone: " + invoice.getCompanyDetails().getPhone() + "\n" + "Email: " + invoice.getCompanyDetails().getEmail() + "\n" +
                "Website: " + invoice.getCompanyDetails().getCompanyWebsite(), fInfo);
        companyAddr.setAlignment(Paragraph.ALIGN_LEFT);

        Paragraph invoiceDetails = new Paragraph("Date: " + invoice.getDate().substring(0, 16) + "\n" + "Invoice Id: " + invoice.getInvoiceId() +
                "\n" + "Due Date: " + invoice.getDueDate(), fInfo);
        invoiceDetails.setAlignment(Paragraph.ALIGN_RIGHT);

       PdfPTable table = new PdfPTable(4);
       table.setWidths(new float[] {150f, 150f, 150f, 150f});
       table.setSpacingBefore(10);

        //writeTableHeader(table);
        writeTableData(table, invoice);
        Paragraph total = new Paragraph("Subtotal: " + invoice.getSubTotal() + '\n' + "Discounts: " + invoice.getDiscounts() + '\n' +
                "Taxes: " + invoice.getTaxes() + '\n' + "Total: " + invoice.getTotal(), fInfo);
        total.setAlignment(Paragraph.ALIGN_RIGHT);

        document.add(header);
        document.add(companyName);
        document.add(companyAddr);
        document.add(invoiceDetails);
        document.add(table);
        document.add(total);
        document.close();

        Path path = Paths.get(fileName);
        Resource resource = null;
        try {
            resource = new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return  ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("text/pdf"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
