package com.amissible.hp.repo;

import com.amissible.hp.model.InvoiceTable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface InvoiceRepository extends ReactiveCrudRepository<InvoiceTable, Long> {
    @Query("SELECT * FROM INVOICE WHERE billed_date = :billedDate")
    Flux<InvoiceTable> findByDate(@Param("billedDate") String billedDate);
}
