package com.amissible.hp.repo;

import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.amissible.hp.model.User;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserRepository  extends ReactiveCrudRepository<User, Long> {

    @Query("SELECT * FROM User WHERE last_name = :lastname")
    Flux<User> findByLastName(String lastName);
    
    @Query("SELECT * FROM User WHERE first_name = :firstName")
    Flux<User> findByName(String firstName);

    @Query("SELECT xid FROM user WHERE email = :email")
    Mono<Long> getXid(String email);

    @Query("SELECT * FROM User WHERE email = :email")
    Mono<User> findByEmail(String email);

    @Query("SELECT * FROM User WHERE xid = :xid")
    Mono<User> findByXid(Long xid);

    @Modifying
    @Query("DELETE from User WHERE email = :email")
    Mono<Integer> deleteByEmail(String email);

    @Modifying
    @Query("UPDATE User SET display_image_url = :displayImageUrl, birth_date = :birth_date, gender = :gender, insurance_provider = :insurance_provider, employer_info = :employer_info, address = :address, real_id = :realId, user_type = :user_type, content = :data WHERE email = :email")
    Mono<Integer> updateByEmail(String email, String displayImageUrl, String birth_date, String gender,
                             String insurance_provider, String employer_info, String address,
                             String realId, String user_type);

    @Query("SELECT * FROM User")
    Flux<User> findAllUsers();
}