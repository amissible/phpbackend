package com.amissible.hp.repo;

import com.amissible.hp.model.InvoiceItemsTable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface InvoiceItemsRepository extends ReactiveCrudRepository<InvoiceItemsTable, Long> {
    @Query("SELECT * FROM INVOICE_ITEMS WHERE invoice_id = :invoiceId")
    Flux<InvoiceItemsTable> findByInvoiceId(@Param("invoiceId") Long invoiceId);
}
