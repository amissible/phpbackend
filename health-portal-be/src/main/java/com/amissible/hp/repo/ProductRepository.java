package com.amissible.hp.repo;

import com.amissible.hp.model.ProductTable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ProductRepository extends ReactiveCrudRepository<ProductTable, Long> {
    @Query("SELECT * FROM PRODUCT WHERE name = :name")
    Flux<ProductTable> findByName(@Param("name") String name);
    @Query("SELECT TOP 1 name FROM PRODUCT WHERE name = :name")
    Flux<ProductTable> checkIfExists(@Param("name") String name);
}
