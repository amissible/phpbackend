package com.amissible.hp.repo;

import com.amissible.hp.model.Pdf;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PdfRepository extends ReactiveCrudRepository<Pdf, Long> {
    @Modifying
    @Query("INSERT INTO PDF (id, filename, content) VALUES(:id, :filename, :content)")
    Flux<Pdf> saveByName(Long id, String filename, byte[] content);

    @Query("SELECT * FROM PDF WHERE filename = :name")
    Flux<Pdf> findByName(String name);

    @Modifying
    @Query("UPDATE PDF SET tags = :tag WHERE filename = :fileName")
    Flux<Integer> updateTags(String tags, String fileName);

    @Query("SELECT * FROM PDF WHERE tags = :tag")
    Flux<Pdf> findAllByTags(String tag);
}
