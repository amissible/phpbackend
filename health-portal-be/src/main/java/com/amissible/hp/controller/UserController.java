package com.amissible.hp.controller;

import com.amissible.hp.CSV.ExportCSV;
import com.amissible.hp.CSV.ImportCSV;
import com.amissible.hp.dao.EmailService;
import com.amissible.hp.model.*;
import com.amissible.hp.pdf.InvoiceExporter;
import com.amissible.hp.pdf.PdfExporter;
import com.amissible.hp.repo.*;
import com.amissible.hp.service.ReportService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import net.sf.jasperreports.engine.JRException;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.mail.MessagingException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    PdfExporter exporter;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserRepository UserRepository;

    @Autowired
    private ExportCSV exportCSV;

    @Autowired
    private ImportCSV importCSV;

    @Autowired
    PdfRepository pdfRepository;

    @Autowired
    InvoiceExporter invoiceExporter;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    InvoiceItemsRepository itemsRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    ReportService reportService;

    private Pdf tmp;

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE, value = "/view-all")
    public Flux<User> findAll() {
        Flux<User> emps = UserRepository.findAll();
        return emps;
    }

    @GetMapping(value = "xid/{xid}")
    public ResponseEntity<Mono<User>> findById(@PathVariable("xid") Long xid) {
        Mono<User> e = UserRepository.findById(xid);
        HttpStatus status = e != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<Mono<User>>(e, status);
    }

    @RequestMapping(value = { "/create"}, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<User> create(@RequestBody User user) throws IOException, MessagingException {
        String subject = "Welcome to health portal";
        String protocol = "http://";
        String deleteLink = protocol + "localhost:8089/user/delete/";
        String body = "Greetings,\n" +
                "Welcome to health portal, use localhost:8089/user/update to update current information," +
                "and localhost:8089/user/delete/{email} to delete your account.\n\n" +
                "Thank you,\n" +
                "Do not reply to this mail." +
                "\n" +
                "In case you did not create this account, use " + deleteLink + user.getEmail() + " to remove your account." ;
        emailService.sendSimpleMessage(user.getEmail(), subject, body);
        return UserRepository.save(user);
    }

    @PostMapping("/update")
    public Mono<Integer> update(@RequestBody User user) {
               return UserRepository.updateByEmail(user.getEmail(), user.getDisplayImageUrl(),
                                user.getBirth_date(), user.getGender(), user.getInsurance_provider(),
                                user.getEmployer_info(), user.getAddress(), user.getRealId(), user.getUser_type());
            }

    @DeleteMapping(value = "/delete/xid/{xid}")
    public Mono<Void> delete(@PathVariable Long xid) {
        return UserRepository.deleteById(xid);
    }

    @DeleteMapping(value = "/delete/email/{email}")
    public Mono<Integer> deleteByEmail(@PathVariable("email") String email) {
              return UserRepository.deleteByEmail(email);
         }

    @GetMapping(value = "email/{email}")
    public ResponseEntity<Mono<User>> findByEmail(@PathVariable("email") String email) {
                Mono<User> e = UserRepository.findByEmail(email);
                HttpStatus status = e != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
                return new ResponseEntity<Mono<User>>(e, status);
            }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public Flux<User> findByName(@PathVariable("name") String name) {
        return UserRepository.findByName(name);
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET)
    public ResponseEntity export(){
        return exportCSV.exportUser();
    }

    @PostMapping(value = "/import",  consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Object uploadCSVFile(@RequestPart("file") FilePart file, Model model) {
        String response = "";
        Object obj = null;
        if (file == null) {
            model.addAttribute("message", "Please select a CSV file to upload.");
            model.addAttribute("status", false);
        } else {
            try{
                CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
                FileReader br = new FileReader(file.filename());
                CSVReader reader = new CSVReaderBuilder(br)
                        .withCSVParser(parser)
                        .build();

                List<String[]> rows = reader.readAll();
                for(int i=1;i<rows.size();i++){
                    String row[] = rows.get(i);
                    try{
                        importCSV.uploadRecord(rows.get(i));
                        response += ",{record:"+i+",status:\"SUCCESS\",message:null}";
                    }
                    catch (Exception e){
                        response += ",{record:"+i+",status:\"FAILURE\",message:\""+e.getMessage()+"\"}";
                    }
                }
                response = "["+response.substring(1)+"]";
                obj = (new JSONParser(response)).parse();
            }
            catch (Exception e){
                System.out.println(e.getStackTrace());
            }
        }
            return obj;
    }

    //returns all pdfs entries after saving (for test purposes).
    @Transactional
    @PostMapping(value = "/upload",  consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Mono<ResponseEntity> uploadPdfFile(@RequestPart("file") FilePart file, Model model) throws Exception {
            String fileName = file.filename();
            if (file == null) {
                model.addAttribute("message", "Please select a Pdf file to upload.");
                model.addAttribute("status", false);
            }
            List<byte[]> b = file.content().map(dataBuffer -> {
                byte[] bytes = new byte[dataBuffer.readableByteCount()];
                dataBuffer.read(bytes);
                DataBufferUtils.release(dataBuffer);
                return bytes;
            }).collectList().block();

            Pdf pdf = new Pdf();
            pdf.setFilename(fileName);
            pdf.setContent(b.get(0));

            pdfRepository.save(pdf).subscribe();
            return Mono.just(new ResponseEntity("Upload successful!", HttpStatus.OK));
    }

    @GetMapping("/download")
    ResponseEntity<Resource> getPdf(@RequestParam String filename) throws IOException {
        tmp = (Pdf) getObjectFromFlux(pdfRepository.findByName(filename));
        if(tmp == null) {
            return new ResponseEntity("File not found!", HttpStatus.NOT_FOUND);
        }
        OutputStream out = new FileOutputStream(filename);
        out.write(tmp.getContent());
        out.close();
        Path path = Paths.get(filename);
        Resource resource = null;
        try {
            resource = new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return  ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("text/pdf"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    //helper function
    <T> Object getObjectFromFlux(Flux<T> flux) {
        List<T> tList = flux.collectList().block();
        return tList.size() < 1 ? null : tList.get(0);
    }

    public <T> List<T> getListFromFlux(Flux<T> flux) {
        List<T> tList = flux.collectList().block();
        return tList.size() < 1 ? null : tList;
    }

    @GetMapping("/update/tags")
    Mono<ResponseEntity> updateTags(@RequestParam String filename, @RequestParam String tag) {
        Integer response = (Integer) getObjectFromFlux(pdfRepository.updateTags(tag, filename));
        if(response == null)
            return Mono.just(new ResponseEntity("Failed to update tag. Make sure the file exists!", HttpStatus.BAD_REQUEST));
        else
            return Mono.just(new ResponseEntity("Tag updated!", HttpStatus.OK));
    }

    @GetMapping("/search/{tag}")
    Flux<Pdf> getAllByTags(@PathVariable String tag) {
        return pdfRepository.findAllByTags(tag);
    }

    @GetMapping ("/view")
    Flux<Pdf> viewAll() {
        return pdfRepository.findAll();
    }

    @GetMapping("/download/user-data")
    ResponseEntity<Resource> getUserData(@RequestParam String email) throws IOException {
        return exporter.export(email);
    }

    @PostMapping("/generateInvoice")
    Mono<ResponseEntity> generateInvoice(@RequestBody InvoicePayload invoicePayload) throws IOException {
        Invoice invoice = new Invoice();
        invoice.setCompanyDetails(invoicePayload.getCompanyDetails());
        invoice.setBillingAddress(invoicePayload.getBillingAddress());
        invoice.setShippingAddress(invoicePayload.getShippingAddress());
        invoice.setProductList(invoicePayload.getProductList());
        return Mono.just(invoiceExporter.export("abc.abc", invoice));
    }

    @PostMapping("/generateInvoice_alpha")
    Mono<ResponseEntity> generateInvoiceAlpha(@RequestBody InvoicePayload invoicePayload) throws IOException, JRException {
        Invoice invoice = new Invoice();
        List<Product> productList = invoicePayload.getProductList();

        //if products dont exist, return bad request, otherwise retrieve their price
       for(Product itr : productList) {
            if((ProductTable)getObjectFromFlux(productRepository.checkIfExists(itr.getDescription())) == null) {
               return Mono.just(new ResponseEntity(HttpStatus.BAD_REQUEST));
            } else {
                ProductTable productTable = (ProductTable) getObjectFromFlux(productRepository.findByName(itr.getDescription()));
                itr.setUnitPrice(productTable.getPrice());
            }
        }
        invoice.setCompanyDetails(invoicePayload.getCompanyDetails());
        invoice.setBillingAddress(invoice.getBillingAddress());
        invoice.setCompanyDetails(invoicePayload.getCompanyDetails());
        invoice.setProductList(productList);

        //create invoice table
        InvoiceTable invoiceTable = new InvoiceTable();
        invoiceTable.setCustomerName(invoice.getCompanyDetails().getCompanyName());
        invoiceTable.setDate(invoice.getDate());
        invoiceTable.setSubTotal(invoice.getSubTotal());
        invoiceTable.setTaxes(invoice.getTaxes());
        invoiceTable.setDiscounts(invoice.getDiscounts());
        invoiceTable.setTotal(invoice.getTotal());
        invoiceRepository.save(invoiceTable).subscribe();
        invoiceTable = (InvoiceTable) getObjectFromFlux(invoiceRepository.findByDate(invoice.getDate()));
        for(Product itr : productList) {
            ProductTable info = (ProductTable) getObjectFromFlux(productRepository.findByName(itr.getDescription()));
            InvoiceItemsTable invoiceItemsTable = new InvoiceItemsTable();
            invoiceItemsTable.setInvoiceId(invoiceTable.getId());
            invoiceItemsTable.setProductId(info.getId());
            invoiceItemsTable.setQuantity(itr.getQuantity());
            itemsRepository.save(invoiceItemsTable).subscribe();
        }
        //try {
            reportService.exportReport("pdf", productList, invoice.getCompanyDetails());
        /*} catch (Exception e)
        {
            return Mono.just(new ResponseEntity(invoice.toString() + "\n" + e.getMessage(), HttpStatus.CREATED));
        }*/
        return Mono.just(new ResponseEntity(invoice.toString(), HttpStatus.CREATED));
    }

    //regenerate invoice from table
    @GetMapping("/view/saved/invoice/{id}")
    Invoice getInvoiceById(@PathVariable Long id) {
        InvoiceTable invoiceTable = invoiceRepository.findById(id).block();
        Invoice invoice = new Invoice();
        invoice.setInvoiceId(invoiceTable.getId());
        invoice.setDate(invoiceTable.getDate());
        invoice.setSubTotal(invoiceTable.getSubTotal());
        invoice.setDiscounts(invoice.getDiscounts());
        invoice.setTaxes(invoiceTable.getTaxes());
        invoice.setTotal(invoice.getTotal());
        invoice.setDueDate(invoiceTable.getDueDate());
        List<InvoiceItemsTable> tableList = getListFromFlux(itemsRepository.findByInvoiceId(id));
        if(tableList.size() < 1)
            return null;
        else {
            List<Product> productList = new ArrayList<>();
            for(InvoiceItemsTable itr : tableList) {
                ProductTable ptable = productRepository.findById(itr.getProductId()).block();
                Product p = new Product();
                p.setDescription(ptable.getName());
                p.setUnitPrice(ptable.getPrice());
                p.setQuantity(itr.getQuantity());
                p.setAmount((ptable.getPrice() * itr.getQuantity()));
                productList.add(p);
            }
            invoice.setProductList(productList);
        }
        return invoice;
    }

    @GetMapping("/view/invoiceItemsTable")
    Flux<InvoiceItemsTable> getAll() {
        return itemsRepository.findAll();
    }

    @GetMapping("/view/invoiceTable")
    Flux<InvoiceTable> getAllInvoice() {
        return invoiceRepository.findAll();
    }

    @PostMapping("/addProduct")
    Mono<ResponseEntity> addProduct(@RequestBody ProductTable product) {
        productRepository.save(product).subscribe();
        return Mono.just(new ResponseEntity(HttpStatus.OK));
    }

    @GetMapping("/getProducts")
    Flux<ProductTable> viewAllProducts() {
        return productRepository.findAll();
    }

    @GetMapping("/searchByName/{name}")
    Flux<ProductTable> viewProducts(@PathVariable String name) {
        return productRepository.findByName(name);
    }
}