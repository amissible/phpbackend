package com.amissible.hp.CSV;

import com.amissible.hp.model.User;
import com.amissible.hp.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ImportCSV {
    @Autowired
    private UserRepository userRepository;

    public String uploadRecord(String row[]){
        for(int i=0;i<row.length;i++) {
            if (row[i] != null)
                row[i] = row[i].trim();
        }
        User user = new User(Long.parseLong(row[0]) + 1L, row[1],
                row[2], row[3], row[4], row[5], row[6], row[7],
                row[8], row[9], row[10], row[11]);
        userRepository.save(user);
        System.out.println(userRepository.findByEmail(user.getEmail()).block());
        return "SUCCESS";
    }
}
