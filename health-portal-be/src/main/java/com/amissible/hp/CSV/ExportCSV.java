package com.amissible.hp.CSV;

import com.amissible.hp.model.User;
import com.amissible.hp.repo.UserRepository;
import com.opencsv.CSVWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class ExportCSV {

    @Autowired
    private UserRepository repository;

    public ResponseEntity exportUser() {

        String fileName = "userrecord.csv";

        try {
            CSVWriter writer = new CSVWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));
            Flux<User> rows =  repository.findAll();
            String header[] = {"xid", "first_name", "lastName", "email", "displayImageUrl", "birth_date", "gender", "insurance_provider",
                    "employer_info", "address", "realId", "user_type"};
            writer.writeNext(header);
            for (User row : rows.toIterable()) {
                String entry[] = {row.getXid().toString(), row.getFirstName(), row.getLastName(), row.getEmail(), row.getDisplayImageUrl(),
                        row.getBirth_date(), row.getGender(), row.getInsurance_provider(), row.getEmployer_info(), row.getAddress(),
                        row.getRealId(), row.getUser_type()};
                writer.writeNext(entry, true);
            }
            writer.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Path path = Paths.get(fileName);
        Resource resource = null;
        try {
            resource = new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return  ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("text/csv"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    /*
     return repository.findById(userId)
        .map(User::asDto)
        .map { ResponseEntity.ok(it) }
        .defaultIfEmpty(ResponseEntity.notFound().build())
     */
}
