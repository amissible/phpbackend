CREATE TABLE USER (XID SERIAL PRIMARY KEY, first_name VARCHAR(255), last_name VARCHAR(255), email VARCHAR(255) NOT NULL UNIQUE, display_image_url VARCHAR(255), birth_date VARCHAR(255), gender VARCHAR(255), insurance_provider VARCHAR(255) , employer_info VARCHAR(255) , address VARCHAR(255),
 real_Id VARCHAR(255), user_type VARCHAR(255));

CREATE TABLE PDF (id SERIAL PRIMARY KEY, filename VARCHAR(255), content BLOB, tags VARCHAR(255));

CREATE TABLE PRODUCT (id SERIAL PRIMARY KEY, name VARCHAR(255), price FLOAT);

CREATE TABLE INVOICE_ITEMS (id SERIAL PRIMARY KEY, invoice_id NUMBER, product_id NUMBER, quantity NUMBER);

CREATE TABLE INVOICE (id SERIAL PRIMARY KEY, customer_name VARCHAR(255), billed_date VARCHAR(255), subtotal FLOAT, discounts FLOAT, taxes FLOAT, total FLOAT, due_date VARCHAR(255));