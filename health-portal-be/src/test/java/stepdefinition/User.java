package stepdefinition;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;

public class User {
    private String firstName;
    private String lastName;
    private String email;
    private String displayImageUrl;
    private String birth_date;
    private String gender;
    private String insurance_provider;
    private String employer_info;
    private String address;
    private String realId;
    private String user_type;

    public User(){

    }

    public User(String firstName, String lastName, String email, String displayImageUrl, String birth_date,
                String gender, String insurance_provider, String employer_info, String address, String realId,
                String user_type) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.displayImageUrl = displayImageUrl;
        this.birth_date = birth_date;
        this.gender = gender;
        this.insurance_provider = insurance_provider;
        this.employer_info = employer_info;
        this.address = address;
        this.realId = realId;
        this.user_type = user_type;
    }


    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getDisplayImageUrl() {
        return displayImageUrl;
    }
    public void setDisplayImageUrl(String displayImageUrl) {
        this.displayImageUrl = displayImageUrl;
    }
    public String getBirth_date() {
        return birth_date;
    }
    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getInsurance_provider() {
        return insurance_provider;
    }
    public void setInsurance_provider(String insurance_provider) {
        this.insurance_provider = insurance_provider;
    }
    public String getEmployer_info() {
        return employer_info;
    }
    public void setEmployer_info(String employer_info) {
        this.employer_info = employer_info;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getRealId() {
        return realId;
    }
    public void setRealId(String realId) {
        this.realId = realId;
    }
    public String getUser_type() {
        return user_type;
    }
    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }
}

