package stepdefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class HealthPortalStepDefinition {
    WebDriver driver;
    RestTemplate restTemplate = new RestTemplate();
    com.amissible.hp.model.User tempUser;

    User user = new User("Sparsh",
            "Mishra",
            "sparshmishra.stud@gmail.com",
            "www.notcool_image.com",
            "15/10/1998",
            "male",
            "Bajaj Allianz",
            "Amissible",
            "Somewhere on Mars",
            "666555",
            "Patient");

    @Given("the website is up")
    public void the_website_is_up(){
        //
    }

    @When("the url is http:\\\\/\\\\/localhost:{int}\\\\/user\\\\/create and the user provides required info")
    public void the_url_is_http_localhost_user_create_and_the_user_provides_required_info(Integer int1) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");
        headers.set("X-COM-LOCATION", "India");

        HttpEntity<User> request = new HttpEntity<>(user, headers);

        ResponseEntity<com.amissible.hp.model.User> res = restTemplate.postForEntity("http://localhost:8089/user/create",
                request, com.amissible.hp.model.User.class);
        tempUser = res.getBody();
        Assert.assertEquals(HttpStatus.CREATED, res.getStatusCode());
    }

    @Then("an email is sent and user is saved.")
    public void an_email_is_sent_and_user_is_saved() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Sparkie\\Downloads\\chromedriver1.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8089/user/get-all-data/pdf?email=" + tempUser.getEmail());

        restTemplate.delete("http://localhost:8089/user/delete/email/" + tempUser.getEmail(), Boolean.class);
    }
}